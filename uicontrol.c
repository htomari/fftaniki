/* UI Control handlers */
#include "glhdrs.h"
#include "fftaniki.h"
#include "logo.h"
#include "plo.h"

static void uic$drawWavBox(FAwin *w,void *wavtab);
static int uic$clickWavBox(FAwin *w,int button, int state, int x, int y, void *arg);
static void uic$drawPlayButton(FAwin *win,void *none);
static int uic$clickPlayButton(FAwin *win,int button,int state,int x,int y, void *arg);
static void uic$drawLogo(FAwin *,void *none);
static void uic$drawHatchBox(FAwin *w,void *none);

/* WavBox */
struct uic$WavBoxMouseCallbackTyp {
	unsigned char *wav;
	void (*update)(wav$editor *arg,unsigned left,unsigned right);
	wav$editor *updateArg;
};
static const int uic$WavBoxH=192;
static const int uic$WavBoxW=512;
extern int uic$regWavBox(FAwin *win) {
	int e;
	win$dispList dispItem;
	win$mouseList mouseItem;
	static struct uic$WavBoxMouseCallbackTyp cb_td,cb_fd;
	/* timespace domain */
	dispItem.enabled=1;
	dispItem.x=0; dispItem.y=0; dispItem.z=0;
	dispItem.dispFunc=&uic$drawWavBox;
	dispItem.arg=win->wav1;
	if((e=win$appendDispList(win,&dispItem))<0) {
		return e;
	}
	mouseItem.enabled=1;
	mouseItem.x0=0; mouseItem.y0=0;
	mouseItem.x1=uic$WavBoxW-1; mouseItem.y1=uic$WavBoxH-1;
	mouseItem.mouseFunc=&uic$clickWavBox;
	cb_td.wav=win->wav1;
	cb_td.update=&wav$timeDomUpdated;
	cb_td.updateArg=win->waved;
	mouseItem.arg=&cb_td;
	if((e=win$appendMouseList(win,&mouseItem))<0) {
		return e;
	}
	/* frequency domain */
	dispItem.y=win->logh-uic$WavBoxH;
	dispItem.arg=win->wav2;
	if((e=win$appendDispList(win,&dispItem))<0) {
		return e;
	}
	mouseItem.y0=win->logh-uic$WavBoxH;
	mouseItem.y1=win->logh;
	cb_fd.wav=win->wav2;
	cb_fd.update=&wav$freqDomUpdated;
	cb_fd.updateArg=win->waved;
	mouseItem.arg=&cb_fd;
	if((e=win$appendMouseList(win,&mouseItem))<0) {
		return e;
	}
	return 0;
}
static void uic$drawWavBox(FAwin*w,void *wavtab) {
	unsigned i;
	unsigned char *ucwavtab=(unsigned char *)wavtab;
	glColor3ubv(win$color[7]);
	glBegin(GL_QUADS);
		glVertex2i(0,0);
		glVertex2i(uic$WavBoxW-1,0);
		glVertex2i(uic$WavBoxW-1,uic$WavBoxH-1);
		glVertex2i(0,uic$WavBoxH-1);
	glEnd();
	glColor3ubv(win$color[0]);
	glBegin(GL_LINES);
		glVertex2i(0,uic$WavBoxH/2);
		glVertex2i(uic$WavBoxW-1,uic$WavBoxH/2);
	glEnd();
	glColor3ubv(win$color[1]);
	glBegin(GL_LINE_STRIP);
	for(i=0; i<512; i++) {
		glVertex2i(i,(GLint) ucwavtab[i+512]);
	}
	glEnd();	
	glColor4ub(0x00,0x00,0xff,0x99);
	glBegin(GL_LINE_STRIP);
	for(i=0; i<512; i++) {
		glVertex2i(i,(GLint) ucwavtab[i]);
	}
	glEnd();
}
static int uic$clickWavBox(FAwin *w,int button, int state, int x, int y, void *arg) {
	struct uic$WavBoxMouseCallbackTyp *cbt=(struct uic$WavBoxMouseCallbackTyp *)arg;
	unsigned char *ucwavtab=cbt->wav;
	static const int szWavTab=512;
	int ix, x1, x2;
	int startx, endx;
	if(w->mouseStatus==win$ms_dragging) {
		x1=uty$min2i(szWavTab-1,uty$max2i(0,w->lastx));
		x2=uty$min2i(szWavTab-1,uty$max2i(0,x));
		startx=uty$min2i(x1,x2);
		endx=uty$max2i(x1,x2);
		for(ix=startx; ix<=endx; ix++) {
			unsigned char newval=uty$min2i(uty$max2i(y,0),uic$WavBoxH-1);
			if(button==GLUT_LEFT_BUTTON || button==GLUT_MIDDLE_BUTTON) {
				ucwavtab[ix]=newval;
			} 
			if(button==GLUT_RIGHT_BUTTON || button==GLUT_MIDDLE_BUTTON) {
				ucwavtab[ix+szWavTab]=newval;
			}
		}
		(*cbt->update)(cbt->updateArg,startx,endx);
	}
	glutPostRedisplay();
	return 1;
}
/* play button */
extern int uic$regPlayButton(FAwin *win) {
	win$dispList dispItem;
	win$mouseList mouseItem;
	int e;
	dispItem.enabled=1;
	dispItem.x=528; dispItem.y=350; dispItem.z=0;
	dispItem.dispFunc=&uic$drawPlayButton;
	dispItem.arg=NULL;
	if((e=win$appendDispList(win,&dispItem))<0) {
		return e;
	}
	mouseItem.enabled=1;
	mouseItem.x0=528; mouseItem.y0=350;
	mouseItem.x1=544; mouseItem.y1=366;
	mouseItem.mouseFunc=&uic$clickPlayButton;
	mouseItem.arg=NULL;
	return win$appendMouseList(win,&mouseItem);
}
static void uic$drawPlayButton(FAwin *win,void *none) {
	glColor3ubv(win$color[1]);
	glBegin(GL_QUADS);
		glVertex2i(0,0);
		glVertex2i(0,16);
		glVertex2i(16,16);
		glVertex2i(16,0);
	glEnd();
}
static int uic$clickPlayButton(FAwin *win,int button,int state,int x,int y, void *arg) {
	if(state==GLUT_DOWN) {
		if(win->sound_on) {
			snd$stopSound(win->snds);
			win->sound_on=0;
		} else {
			win->sound_on=1;
			snd$copyBuf(win->snds,win->wav1);
			snd$startSound(win->snds);
		}
	}
	return 1;
}
/* Logo */
static GLuint uic$logoName[2];
extern int uic$regLogo(FAwin *win) {
	win$dispList dispItem;
	dispItem.enabled=1;
	dispItem.x=512; dispItem.y=0; dispItem.z=0;
	dispItem.dispFunc=&uic$drawLogo;
	dispItem.arg=NULL;
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glGenTextures(2,uic$logoName);
	glBindTexture(GL_TEXTURE_2D,uic$logoName[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
			GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
			GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128,
		     0, GL_RGB, GL_UNSIGNED_BYTE, 
		     uic$logo);
	glBindTexture(GL_TEXTURE_2D,uic$logoName[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
			GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
			GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128,
		     0, GL_RGB, GL_UNSIGNED_BYTE, 
		     uic$plo);
	return win$appendDispList(win,&dispItem);
}
static void uic$drawLogo(FAwin *win,void *none) {
	unsigned logoid;
	logoid=win->secret;
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glBindTexture(GL_TEXTURE_2D, uic$logoName[logoid]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0,0.0); glVertex2i(0,0);
	glTexCoord2f(0.0,1.0); glVertex2i(0,127);
	glTexCoord2f(1.0,1.0); glVertex2i(127,127);
	glTexCoord2f(1.0,0.0); glVertex2i(127,0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}
/* HatchBox (for debug) */
extern int uic$regHatchBox(FAwin *win) {
	win$dispList dispItem;
	dispItem.enabled=1;
	dispItem.x=0; dispItem.y=0; dispItem.z=0;
	dispItem.dispFunc=&uic$drawHatchBox;
	dispItem.arg=NULL;
	return win$appendDispList(win,&dispItem);
}
static void uic$drawHatchBox(FAwin *w,void *none) {
	glColor3ubv(win$color[1]);
	glBegin(GL_LINES);
		glVertex2i(0,0);
		glVertex2i(w->logw-1,w->logh-1);
	glEnd();
	glColor3ubv(win$color[2]);
	glBegin(GL_LINES);
		glVertex2i(w->logw-1,0);
		glVertex2i(0,w->logh-1);
	glEnd();
	glColor3ubv(win$color[4]);
	glBegin(GL_LINE_LOOP);
		glVertex2i(0,0);
		glVertex2i(w->logw-1,0);
		glVertex2i(w->logw-1,w->logh-1);
		glVertex2i(0,w->logh-1);
	glEnd();
}
