/* FFT aniki: FFT toy
   2013 H.Tomari */
#include "fftaniki.h"

extern int main(int argc, char *argv[]);
static void loc$cleanup(void);

static wav$editor we;
static snd$state ss;

void loc$cleanup() {
	wav$destroy(&we);
	snd$destroy(&ss);
}
int main(int argc, char *argv[]) {
	FAwin win;
	wav$init(&we,win.wav1,win.wav2);
	snd$init(&ss,&argc,argv);
	win$init(&win,&we,&ss,&argc,argv,640,400);
	atexit(&loc$cleanup);
	win$evtLoop(&win);
	return EXIT_SUCCESS;
}
