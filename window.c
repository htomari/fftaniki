/* GLUT bridge */
#include "glhdrs.h"
#include "fftaniki.h"

static const char *win$title="FFT Aniki";
const unsigned char win$color[][3] = {
	{0x00,0x00,0x00},
	{0xff,0x00,0x00},
	{0x00,0xff,0x00},
	{0x00,0x00,0xff},
	{0xff,0xff,0x00},
	{0x00,0xff,0xff},
	{0xff,0x00,0xff},
	{0xff,0xff,0xff}
};

static int win$regUIelements(FAwin *win);
static void win$disp(void);
static void win$keyboard(unsigned char key,int x,int y);
static int win$transMouseX(FAwin *win,int x);
static int win$transMouseY(FAwin *win,int y);
static void win$mouse(int button,int mousestate,int x,int y);
static void win$motion(int x,int y);
static void win$reshape(int w,int h);

static FAwin* win$FAwin;

extern int win$appendDispList(FAwin *win,win$dispList *dispItem) {
	unsigned i;
	for(i=0;i<(sizeof(win->dispList)/sizeof(win$dispList));i++) {
		if(!win->dispList[i].enabled) {
			win->dispList[i]=*dispItem;
			return i;
		}
	}
	return -1;
}
extern int win$appendMouseList(FAwin *win,win$mouseList *mouseItem) {
	unsigned i;
	for(i=0;i<(sizeof(win->mouseList)/sizeof(win$mouseList));i++) {
		if(!win->mouseList[i].enabled) {
			win->mouseList[i]=*mouseItem;
			return i;
		}
	}
	return -1;
}
static int win$regUIelements(FAwin *win) {
	uic$regHatchBox(win);
	uic$regWavBox(win);
	uic$regPlayButton(win);
	uic$regLogo(win);
	return 0;
}
static void win$disp() {
	unsigned i;
	glClear(GL_COLOR_BUFFER_BIT);
	for(i=0;i<(sizeof(win$FAwin->dispList)/sizeof(win$dispList));i++) {
		if(win$FAwin->dispList[i].enabled) {
			glPushMatrix();
			glTranslatef((GLfloat)win$FAwin->dispList[i].x,
				     (GLfloat)win$FAwin->dispList[i].y,
				     (GLfloat)win$FAwin->dispList[i].z);
			(*(win$FAwin->dispList[i].dispFunc))(win$FAwin,win$FAwin->dispList[i].arg);
			glPopMatrix();
		}
	}
	glutSwapBuffers();
}
static void win$keyboard(unsigned char key,int x,int y) {
	switch(key) {
	case 's':
		win$FAwin->secret=win$FAwin->secret?0:1;
		glutPostRedisplay();
		break;
	case 'q':
	case 'Q':
	case '\033': /* ESCAPE key */
		exit(EXIT_SUCCESS);
		break;
	default:
		break;
	}
}
static int win$transMouseX(FAwin *win, int x) {
	return win->scale*(float)(x-win->offsw);
}
static int win$transMouseY(FAwin *win, int y) {
	return win->scale*(float)(y-win->offsh);
}
static void win$mouse(int button,int mousestate,int x,int y) {
	unsigned i;
	int transx,transy,passx,passy;
	int handled;
	transx=win$transMouseX(win$FAwin,x);
	transy=win$transMouseY(win$FAwin,y);
	if(mousestate==GLUT_UP) {
		win$FAwin->mouseStatus=win$ms_free;
		win$FAwin->lastobj=-1;
	}
	for(i=0;i<(sizeof(win$FAwin->mouseList)/sizeof(win$mouseList));i++) {
		win$mouseList *ml=&win$FAwin->mouseList[i];
		if(ml->enabled) {
			if(ml->x0<=transx && ml->y0<=transy &&
			   transx<=ml->x1 && transy<=ml->y1) {
				passx=transx-ml->x0;
				passy=transy-ml->y0;
				handled=(*(ml->mouseFunc))(win$FAwin,button,mousestate,passx,passy,ml->arg);
				if(handled) {
					if(mousestate==GLUT_DOWN) {
						win$FAwin->mouseStatus=win$ms_dragging;
						win$FAwin->lastx=passx;
						win$FAwin->lasty=passy;
						win$FAwin->lastbutton=button;
						win$FAwin->lastobj=i;
					}
					return;
				}
			}
		}
	}
}
static void win$motion(int x,int y) {
	int passx,passy,handled;
	if(win$FAwin->lastobj>=0) {
		win$mouseList *ml=&win$FAwin->mouseList[win$FAwin->lastobj];
		if(ml->enabled) {
			passx=win$transMouseX(win$FAwin,x)-ml->x0;
			passy=win$transMouseY(win$FAwin,y)-ml->y0;
			handled=(*(ml->mouseFunc))(win$FAwin,win$FAwin->lastbutton,GLUT_DOWN,passx,passy,ml->arg);
			if(handled) {
				win$FAwin->lastx=passx;
				win$FAwin->lasty=passy;
			}
		} else {
			win$FAwin->mouseStatus=win$ms_free;
			win$FAwin->lastobj=-1;
		}
	}
}
static void win$reshape(int w,int h) {
	float scalew,scaleh;
	float multw=1.0f,multh=1.0f;
	win$FAwin->winw=w;
	win$FAwin->winh=h;
	glViewport(0,0,w,h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	scalew=((float)w)/(float)win$FAwin->logw;
	scaleh=((float)h)/(float)win$FAwin->logh;
	if(scalew>scaleh) {
		multw=scaleh/scalew;
		win$FAwin->scale=1.0f/scaleh;
		win$FAwin->offsh=0;
		win$FAwin->offsw=(int)((((float)w)-scaleh*((float)win$FAwin->logw))/2.0f);
		win$FAwin->offsh=0;
	} else {
		multh=scalew/scaleh;
		win$FAwin->scale=1.0f/scalew;
		win$FAwin->offsh=(int)((((float)h)-scalew*((float)win$FAwin->logh))/2.0f);
		win$FAwin->offsw=0;
	}
	glScalef(multw*2.0/(float)win$FAwin->logw,multh*-2.0/(float)win$FAwin->logh,0.0f);
	glTranslatef(((float)win$FAwin->logw)/-2.0f,((float)win$FAwin->logh)/-2.0f,0.0f);
}
extern int win$init(FAwin *win, wav$editor *waved, snd$state *ss,int *argc, char *argv[], int w, int h) {
	unsigned i;
	int e;
	win$FAwin=win;
	win$FAwin->logw=w;
	win$FAwin->logh=h;
	win$FAwin->mouseStatus=win$ms_free;
	win$FAwin->waved=waved;
	win$FAwin->snds=ss;
	win$FAwin->sound_on=0;
	win$FAwin->secret=0;
	for(i=0;i<(sizeof(win->mouseList)/sizeof(win$mouseList));i++) {
		win->mouseList[i].enabled=0;
	}
	for(i=0;i<(sizeof(win->dispList)/sizeof(win$dispList));i++) {
		win->dispList[i].enabled=0;
	}
	glutInitWindowSize(w,h);
	glutInit(argc,argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE);
	glutCreateWindow(win$title);
	if((e=win$regUIelements(win))<0) {
		return e;
	}
	glutDisplayFunc(&win$disp);
	glutKeyboardFunc(&win$keyboard);
	glutMouseFunc(&win$mouse);
	glutMotionFunc(&win$motion);
	glutReshapeFunc(&win$reshape);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	return 0;
}
extern int win$evtLoop(FAwin *win) {
	glutMainLoop();
	return 0;
}

