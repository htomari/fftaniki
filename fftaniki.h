#include <stdlib.h>
#include <stdio.h>

/* Wave */
struct wav$fftwork;
typedef struct {
	unsigned char *tWavI;
	unsigned char *fWavI;
	struct wav$fftwork *fw;
} wav$editor;
extern int wav$init(wav$editor *e,unsigned char *twavi,unsigned char *fwavi);
extern void wav$destroy(wav$editor *e);
extern void wav$timeDomUpdated(wav$editor *arg,unsigned left,unsigned right);
extern void wav$freqDomUpdated(wav$editor *arg,unsigned left,unsigned right);

/* Audio */
typedef struct {
	unsigned int abuf,asrc;
} snd$state;
extern int snd$init(snd$state *ss,int *argc, char *argv[]);
extern void snd$startSound(snd$state *ss);
extern void snd$stopSound(snd$state *ss);
extern void snd$copyBuf(snd$state *ss, unsigned char *from);
extern void snd$destroy(snd$state *ss);

/* Window */
struct FAwin;
typedef struct {
	unsigned enabled;
	int x0,y0;
	int x1,y1;
	int (*mouseFunc)(struct FAwin *w,int button, int state, int x, int y, void *farg);
	void *arg;
} win$mouseList;
typedef struct {
	unsigned enabled;
	int x,y,z;
	void (*dispFunc)(struct FAwin *w,void *farg);
	void *arg;
} win$dispList;
enum win$mouseStatus {
	win$ms_free,
	win$ms_dragging
};
struct FAwin {
	win$dispList dispList[32];
	win$mouseList mouseList[32];
	unsigned char wav1[1024];
	unsigned char wav2[1024];
	int logw,logh;
	int winw,winh,offsw,offsh;
	float scale;
	enum win$mouseStatus mouseStatus;
	int lastx, lasty, lastbutton, lastobj;
	int sound_on;
	wav$editor *waved;
	snd$state *snds;
	unsigned secret;
};
typedef struct FAwin FAwin;
extern const unsigned char win$color[][3];

extern int win$appendDispList(FAwin *win,win$dispList *dispItem);
extern int win$appendMouseList(FAwin *win,win$mouseList *mouseItem);
extern int win$init(FAwin *win, wav$editor *waved, snd$state *ss,int *argc, char *argv[],int logw,int logh);
extern int win$evtLoop(FAwin *win);

/* UI controls */
extern int uic$regWavBox(FAwin *win);
extern int uic$regPlayButton(FAwin *win);
extern int uic$regLogo(FAwin *win);
extern int uic$regHatchBox(FAwin *win);

/* Utility */
static inline int uty$min2i(int x,int y) { return x<y?x:y; }
static inline int uty$max2i(int x,int y) { return x<y?y:x; }
