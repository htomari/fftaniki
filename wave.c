/* waveform editor */
#include "fftaniki.h"
#include <complex.h>
#include <math.h>
#include <fftw3.h>

static const unsigned wav$len=512;
static const int wav$ihigh=0;
static const int wav$ilow=191;
struct wav$fftwork {
	fftw_complex *out,*tWav,*fWav;
	fftw_plan TtoF, FtoT;
};
typedef enum {
	wav$pm_real,
	wav$pm_imag
} wav$projMode;
static void wav$ItoC(unsigned char *i,complex *d,unsigned count);
static void wav$CtoI(complex *d,unsigned char *i,unsigned count);
static void wav$fftTtoF(wav$editor *e);
static void wav$fftFtoT(wav$editor *e);

static void wav$ItoC(unsigned char *i,complex *d,unsigned count) {
	unsigned j;
	const double pp2=(wav$ilow-wav$ihigh+1)/2;
	const double TwoOvrPP=1./(double)pp2;
	double i11,i11i;
	for(j=0; j<count; j++) {
		i11=(pp2-(double)i[j])*TwoOvrPP;
		i11i=(pp2-(double)i[j+wav$len])*TwoOvrPP;
		d[j]=i11+I*i11i;
	}
}
static void wav$CtoI(complex *d,unsigned char *i,unsigned count) {
	unsigned j;
	const double pp2=(double) ((wav$ilow-wav$ihigh+1)/2);
	for(j=0; j<count; j++) {
		i[j]=pp2-pp2*creal(d[j]);
		i[j+wav$len]=pp2-pp2*cimag(d[j]);
	}
}
static void wav$fftTtoF(wav$editor *e) {
	unsigned i;
	const double norm=1./sqrt(wav$len);
	fftw_execute(e->fw->TtoF);
	for(i=0; i<wav$len; i++) {
		e->fw->fWav[i]=norm*e->fw->out[i];
	}
}
static void wav$fftFtoT(wav$editor *e) {
	unsigned i;
	const double norm=1./sqrt(wav$len);
	fftw_execute(e->fw->FtoT);
	for(i=0; i<wav$len; i++) {
		e->fw->tWav[i]=norm*e->fw->out[i];
	}
}
extern int wav$init(wav$editor *e,unsigned char *twavi, unsigned char *fwavi) {
	unsigned i;
	int result=-1;
	e->tWavI=twavi;
	e->fWavI=fwavi;
	if(!(e->fw=calloc(sizeof(struct wav$fftwork),1))) { goto abort; }
	if(!(e->fw->tWav=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*wav$len))) {
		free(e->fw);
		goto abort;
	}
	if(!(e->fw->fWav=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*wav$len))) {
		fftw_free(e->fw->tWav);
		free(e->fw);
		goto abort;
	}
	if(!(e->fw->out=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*wav$len))) {
		fftw_free(e->fw->tWav);
		fftw_free(e->fw->fWav);
		free(e->fw);
		goto abort;
	}
	for(i=0; i<wav$len; i++) {
		e->fw->tWav[i]=0.+0.*I;
		e->fw->fWav[i]=0.+0.*I;
	}
	wav$CtoI(e->fw->tWav,e->tWavI,wav$len);
	wav$CtoI(e->fw->fWav,e->fWavI,wav$len);
	e->fw->TtoF=fftw_plan_dft_1d(wav$len,e->fw->tWav,e->fw->out,FFTW_FORWARD,FFTW_ESTIMATE);
	e->fw->FtoT=fftw_plan_dft_1d(wav$len,e->fw->fWav,e->fw->out,FFTW_BACKWARD,FFTW_ESTIMATE);
	result=0;
abort:
	return result;
}
extern void wav$destroy(wav$editor *e) {
	fftw_destroy_plan(e->fw->FtoT);
	fftw_destroy_plan(e->fw->TtoF);
	fftw_free(e->fw->out);
	fftw_free(e->fw->fWav);
	fftw_free(e->fw->tWav);
	free(e->fw);
}
extern void wav$timeDomUpdated(wav$editor *arg,unsigned left,unsigned right) {
	wav$ItoC(&(arg->tWavI[left]),&(arg->fw->tWav[left]),right-left+1);
	wav$fftTtoF(arg);
	wav$CtoI(arg->fw->fWav,arg->fWavI,wav$len);
}
extern void wav$freqDomUpdated(wav$editor *arg,unsigned left,unsigned right) {
	wav$ItoC(&(arg->fWavI[left]),&(arg->fw->fWav[left]),right-left+1);
	wav$fftFtoT(arg);
	wav$CtoI(arg->fw->tWav,arg->tWavI,wav$len);
}
