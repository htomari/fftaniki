/* audio output */
#include "fftaniki.h"
#include <AL/alut.h>

extern int snd$init(snd$state *ss,int *argc, char *argv[]) {
	ALuint sndbuf,sndsrc;
	ALenum err;
	alutInit(argc,argv);
	alGenBuffers(1,&sndbuf);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alGenBuffers");
	}	
	ss->abuf=sndbuf;
	alGenSources(1,&sndsrc);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alGenSources");
	}	
	ss->asrc=sndsrc;
	alSourcei(sndsrc,AL_BUFFER,sndbuf);
	alSourcei(sndsrc,AL_LOOPING,AL_TRUE);
	alSourcef(sndsrc,AL_GAIN,0.7f);
	return 0;
}
extern void snd$startSound(snd$state *ss) {
	ALenum err;
	ALuint sndsrc=ss->asrc;
	alSourcePlay(sndsrc);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alSourcePlay");
	}
}
extern void snd$stopSound(snd$state *ss) {
	ALenum err;
	ALuint sndsrc=ss->asrc;
	alSourceStop(sndsrc);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alSourceStop");
	}
}
extern void snd$copyBuf(snd$state *ss, unsigned char *from) {
	ALuint abuf=ss->abuf;
	ALuint asrc=ss->asrc;
	unsigned char norm[512];
	unsigned int i;
	ALenum err;
	for(i=0; i<512; i++) {
		norm[i]=128+(192/2-from[i]);
	}
	alDeleteSources(1,&asrc);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alDeleteSources");
	}
	alBufferData(abuf,AL_FORMAT_MONO8,norm,sizeof(norm),512*220);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alBufferData");
	}
	alGenSources(1,&asrc);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alGenSources");
	}
	alSourcei(asrc,AL_BUFFER,abuf);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alSourcei AL_BUFFER");
	}
	alSourcei(asrc,AL_LOOPING,AL_TRUE);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alSourcei AL_LOOPING");
	}
	alSourcef(asrc,AL_GAIN,0.7f);
	if((err=alGetError())!=AL_NO_ERROR) {
		puts("alSourcef AL_GAIN");
	}
	ss->asrc=asrc;
}
extern void snd$destroy(snd$state *ss) {
	alutExit();
}
