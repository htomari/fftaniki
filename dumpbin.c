/* dump binary into c array */
#include <stdlib.h>
#include <stdio.h>
static void usage(void);
static void usage() {
	fputs("dumpbin input\n",stderr);
	exit(EXIT_FAILURE);
}
extern int main(int argc, char *argv[]) {
	FILE *in;
	long len,i;
	int res=EXIT_FAILURE;
	if(argc<2) { usage(); }
	if(!(in=fopen(argv[1],"r"))) {
		perror("fopen");
		return res;
	}
	if(fseek(in,0L,SEEK_END)) {
		perror("fseek");
		goto abort;
	}
	if((len=ftell(in))<0) {
		perror("ftell");
		goto abort;
	}
	if(fseek(in,0L,SEEK_SET)) {
		perror("fseek");
		goto abort;
	}
	printf("static const unsigned char binary[%ld]={",len);
	for(i=0L; i<len; i++) {
		unsigned char c;
		if(!(i&0xf)) { putchar('\n'); }
		if(!(fread(&c,sizeof(unsigned char),1,in))) {
			perror("fread");
			goto abort;
		}
		printf("0x%02x,",c);
	}
	puts("};\n");
	res=EXIT_SUCCESS;
abort:
	fclose(in);
	return res;
}

